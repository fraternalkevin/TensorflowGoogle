import tensorflow as tf
import numpy as np
layer_number = 2
# 创建输入数据,3代表batch size,6代表输入序列的最大步长(max time),4代表每个序列的维度
X = np.random.randn(3, 6, 4)
# 第二个输入的实际长度为4
X[1, 4:] = 0
# 记录三个输入的实际步长
X_lengths = [6, 4, 6]
rnn_hidden_size = 5
cell = tf.nn.rnn_cell.MultiRNNCell([tf.nn.rnn_cell.BasicLSTMCell(rnn_hidden_size) for _ in range(layer_number)])
# cell = tf.nn.rnn_cell.BasicRNNCell(rnn_hidden_size)
outputs, last_states = tf.nn.dynamic_rnn(
    cell=cell,
    dtype=tf.float64,
    sequence_length=X_lengths,
    inputs=X)

with tf.Session() as session:
    session.run(tf.global_variables_initializer())
    output, state = session.run([outputs, last_states])
    print(np.shape(output))
    print(output)
    print(np.shape(state))
    print(state)
