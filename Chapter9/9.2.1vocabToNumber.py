import codecs
import sys

RAW_DATA = './simple-examples/data/ptb.valid.txt'
VOCAB = './simple-examples/data/ptb.vocab'
OUTPUT_DATA = 'ptb.valid.txt'  # 单词替换成单词编码的输出文件
with codecs.open(VOCAB, 'r', 'utf-8') as f_vacab:
    vocab = [w.strip() for w in f_vacab.readlines()]
word_to_id = {k: v for (k, v) in zip(vocab, range(len(vocab)))}
print(word_to_id)


def get_id(word):
    return word_to_id[word] if word in word_to_id else word_to_id['<unk>']


fin = codecs.open(RAW_DATA, 'r', 'utf-8')
fout = codecs.open(OUTPUT_DATA, 'w', 'utf-8')
for line in fin:
    words = line.strip().split() + ['<eos>']
    #在每个单词之间添加空格
    out_line = ' '.join([str(get_id(w)) for w in words]) + '\n'
    fout.write(out_line)
fin.close()
fout.close()