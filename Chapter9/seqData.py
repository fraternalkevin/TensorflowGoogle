import codecs
import collections
from operator import itemgetter

RAW_DATA = './simple-examples/data/ptb.train.txt'
VOCAB_OUTPUT = './ptb.vocab'

counter = collections.Counter()
with codecs.open(RAW_DATA, 'r', 'utf-8') as f:
    for line in f:
        for word in line.strip().split():
            counter[word] += 1
sorted_word_to_cnt = sorted(counter.items(), key=itemgetter(1), reverse=True)

sorted_word = [x[0] for x in sorted_word_to_cnt]
sorted_word = ['<eos>'] + sorted_word

with codecs.open(VOCAB_OUTPUT,'w','utf-8') as file_output:
    for word in sorted_word:
        file_output.write(word+'\n')