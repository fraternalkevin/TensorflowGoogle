import tensorflow as tf

x = tf.placeholder(shape=tf.TensorShape([]), dtype=tf.float32)
y = tf.placeholder(shape=tf.TensorShape([None]), dtype=tf.float32)
sess = tf.Session()
sess.run(tf.global_variables_initializer())
print(sess.run([x, y], feed_dict={x: 5.0, y:[5.0, 5.0, 5.0]}))
