import tensorflow as tf
import numpy as np

image1 = np.zeros([64, 64, 3],dtype=np.float32)
image2 = np.zeros([64, 64, 3],dtype=np.float32)


def conv_relu(input, kernel_shape, bias_shape):
    # Create variable named "weights".
    weights = tf.get_variable("weights", kernel_shape,
                              initializer=tf.random_normal_initializer())
    # Create variable named "biases".
    biases = tf.get_variable("biases", bias_shape,
                             initializer=tf.constant_initializer(0.0))
    conv = tf.nn.conv2d(input, weights,
                        strides=[1, 1, 1, 1], padding='SAME')
    return tf.nn.relu(conv + biases)


def my_image_filter(input_images):
    with tf.variable_scope("conv1"):
        # Variables created here will be named "conv1/weights", "conv1/biases".
        # relu1 = conv_relu(input_images, [5, 5, 32, 32], [32])
        relu1 = tf.get_variable("weights", [1],
                              initializer=tf.random_normal_initializer())
    with tf.variable_scope("conv2"):
        # Variables created here will be named "conv2/weights", "conv2/biases".
        # return conv_relu(relu1, [5, 5, 32, 32], [32])
        return 2+relu1


with tf.variable_scope("image_filters") as scope:
    result1 = my_image_filter(image1)
    scope.reuse_variables()
    result2 = my_image_filter(image2)
