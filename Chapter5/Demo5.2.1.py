import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
from matplotlib import pyplot
import numpy
INPUT_NODE = 784
OUT_NODE = 10
LAYDER1_NODE = 500
BATCH_SIZE = 100
LEARNING_RATE_BASE = 0.8
LEARNING_RATE_DECAY = 0.99
REGULARIZATION_RATE = 0.0001
TRAININGS_STEPS = 3000
MOVINE_AVERAGE_DECAY = 0.99
pyplot_list = list()

def inference(input_tensor,avg_class,weights1,biasses1,weights2,biasses2):
    if avg_class == None:
        layer1 = tf.nn.relu(tf.matmul(input_tensor,weights1)+biasses1)
        return tf.matmul(layer1,weights2)+biasses2
    else:
        layer1 = tf.nn.relu(tf.matmul(input_tensor,avg_class.average(weights1))+avg_class.average(biasses1))
        return tf.matmul(layer1,avg_class.average(weights2))+avg_class.average(biasses2)
def tarin(mnist):
    x = tf.placeholder(dtype=tf.float32,shape=[None,INPUT_NODE],name='x_input')
    y_ = tf.placeholder(dtype=tf.float32, shape=[None, OUT_NODE],name='y_input')
    weights1 = tf.Variable(tf.truncated_normal(shape=[INPUT_NODE,LAYDER1_NODE],stddev=0.1))
    biases1 = tf.Variable(tf.constant(0.1,shape=[LAYDER1_NODE]))
    weights2 = tf.Variable(tf.truncated_normal(shape=[LAYDER1_NODE, OUT_NODE], stddev=0.1))
    biases2 = tf.Variable(tf.constant(0.1, shape=[OUT_NODE]))
    y = inference(x,None,weights1,biases1,weights2,biases2)

    global_step = tf.Variable(0,trainable=False)
    variable_average = tf.train.ExponentialMovingAverage(MOVINE_AVERAGE_DECAY,global_step)
    variable_average_op = variable_average.apply(tf.trainable_variables())
    average_y = inference(x,variable_average,weights1,biases1,weights2,biases2)
    cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=y,labels=tf.argmax(y_,1))
    cross_entropy_mean = tf.reduce_mean(cross_entropy)
    regularizer = tf.contrib.layers.l2_regularizer(REGULARIZATION_RATE)
    regularization = regularizer(weights1)+regularizer(weights2)
    loss = regularization+cross_entropy_mean
    learning_rate = tf.train.exponential_decay(LEARNING_RATE_BASE,global_step,mnist.train.num_examples/BATCH_SIZE,LEARNING_RATE_DECAY)
    train_step = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss,global_step=global_step)
    with tf.control_dependencies([train_step,variable_average_op]):
        train_op = tf.no_op(name='train')
    correct_predict = tf.equal(tf.argmax(average_y,1),tf.argmax(y_,1))
    accuracy = tf.reduce_mean(tf.cast(correct_predict,dtype=tf.float32))

    saver = tf.train.Saver(max_to_keep=2)
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        validate_feed = {x:mnist.validation.images,y_:mnist.validation.labels}
        test_feed = {x:mnist.test.images,y_:mnist.test.labels}
        for i in range(TRAININGS_STEPS):
            if i%1000 ==0:
                validate_accuracy = sess.run(accuracy,feed_dict=validate_feed)

                print("afer %d training step(s),validation accurace "
                      "using average model is %g"%(i,validate_accuracy))
                saver.save(sess,save_path='model/my_model',global_step=i)
                pyplot_list.append([i,validate_accuracy])
                # pyplot.plot(i % 1000, validate_accuracy)
                # pyplot.show()
            xs,ys = mnist.train.next_batch(BATCH_SIZE)
            sess.run(train_op,feed_dict={x:xs,y_:ys})

        test_acc = sess.run(accuracy,feed_dict=test_feed)
        print("afer %d training step(s),test accurace "
              "using average model is %g" % (TRAININGS_STEPS, test_acc))
        print(pyplot_list)
        pyplot_array = numpy.array(pyplot_list)
        pyplot.plot(pyplot_array[:,0],pyplot_array[:,1])
        pyplot.show()

def main(argv = None):
    mnist = input_data.read_data_sets('/DataSets',one_hot=True)
    tarin(mnist)
if __name__ == '__main__':
    tf.app.run()