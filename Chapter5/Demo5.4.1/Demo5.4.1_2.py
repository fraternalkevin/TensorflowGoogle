import tensorflow as tf
v3 = tf.Variable(tf.constant(1.0,shape=[1]),name='v3')
v4 = tf.Variable(tf.constant(2.0,shape=[1]),name='v4')
result = v3+v4
saver = tf.train.Saver({'v1':v3,'v2':v4})
with tf.Session() as sess:
    saver.restore(sess,save_path='model5.4.1/model.ckpt')
    print(sess.run(result))