import numpy as np
from keras.callbacks import TensorBoard
np.random.seed(1337)  # for reproducibility
from keras.layers import Dense, Input,LSTM,RepeatVector
from keras import regularizers
from keras.models import Model
import numpy as np
import matplotlib.pyplot as plt

import os

path = 'E:\A_PycharmProjects\LearningDemo\MNIST_data\mnist .npz'
file = np.load(path)
x_train, y_train = file['x_train'], file['y_train']
x_test, y_test = file['x_test'], file['y_test']
x_train = x_train.astype('float32') / 255.
x_test = x_test.astype('float32') / 255.
x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))
print(x_train.shape)
print(x_test.shape)








# 编码维度
encoding_dim = 32
# this is our input placeholder
input_img = Input(shape=(784,))
# 搭建编码器
encoded = Dense(128, activation='relu')(input_img)
encoded = Dense(64,activation='relu')(encoded)
encoded = Dense(32,activation='relu')(encoded)
# 搭建解码器
decoded = Dense(64,activation='relu')(encoded)
decoded = Dense(128,activation='relu')(encoded)
decoded = Dense(784, activation='sigmoid')(encoded)

# this model maps an input to its reconstruction
autoencoder = Model(input=input_img, output=decoded)
#定义损失函数
autoencoder.compile(optimizer='adadelta', loss='binary_crossentropy')
autoencoder.fit(x_train, x_train,
                epochs=5,
                batch_size=256,
                shuffle=True,
                validation_data=(x_test, x_test),callbacks=[TensorBoard(log_dir='/tmp/autoencoder')])
#模型拟合之后的使用
decoded_imgs = autoencoder.predict(x_test)

n = 10  # how many digits we will display
plt.figure(figsize=(20, 4))
for i in range(n):
    ax = plt.subplot(2, n, i + 1)
    plt.imshow(x_test[i].reshape(28, 28))
    plt.gray()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

    ax = plt.subplot(2, n, i + 1 + n)
    plt.imshow(decoded_imgs[i].reshape(28, 28))
    plt.gray()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
plt.show()