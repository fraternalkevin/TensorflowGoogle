import tensorflow as tf
from numpy.random import RandomState
batch_size = 8
w1 = tf.Variable(tf.random_normal([2,3],stddev=1,dtype=tf.float32,seed=1))
w2 = tf.Variable(tf.random_normal([3,1],stddev=1,dtype=tf.float32,seed=1))
x = tf.placeholder(dtype=tf.float32,shape=[None,2],name='x_input')
y_ = tf.placeholder(dtype=tf.float32,shape=[None,1],name='y_input')
#定义神经网络的前向传播
a = tf.matmul(x,w1)
y = tf.matmul(a,w2)
y = tf.sigmoid(y)
learning_rate = 0.1
global_ = tf.Variable(tf.constant(0))
decay_steps = batch_size
decay_rate = 0.96
learning_rate = tf.train.exponential_decay(learning_rate, global_, decay_steps, decay_rate, staircase=True)
cross_entropy = - tf.reduce_mean(y_*tf.log(tf.clip_by_value(y,1e-10,1.0))+(1-y_)*tf.log(tf.clip_by_value(1-y,1e-10,1.0)))
train_step = tf.train.AdamOptimizer(learning_rate).minimize(cross_entropy)

rmd = RandomState(1)
dataset_size = 128
X = rmd.rand(dataset_size,2)
Y = [[int (x1 + x2 < 1)] for (x1,x2) in X]

with tf.Session() as sess:
    init_op = tf.global_variables_initializer()
    sess.run(init_op)
    print(sess.run(w1))
    print(sess.run(w2))
    steps = 5000
    for i in range(steps):
        start = (i*batch_size)%dataset_size
        end = min(start+batch_size,dataset_size)
        sess.run(train_step,feed_dict={x:X[start:end],y_:Y[start:end]})
        if i % 1000 ==0:
            total_cross_entropy = sess.run(cross_entropy,feed_dict={x:X,y_:Y})
            print("after %d training step(s),corss entropy on all data is %g"%(i,total_cross_entropy))
    print(sess.run(w1))
    print(sess.run(w2))



