import tensorflow as tf
from numpy.random import RandomState

def get_weight(shape,lamda):
    var = tf.Variable(tf.random_normal(shape=shape),dtype=tf.float32)
    tf.add_to_collection('losses',tf.contrib.layers.l1_regularizer(lamda)(var))
    return var
x = tf.placeholder(dtype=tf.float32,shape=[None,2],name='x_input')
y_ = tf.placeholder(dtype=tf.float32,shape=[None,1],name='y_input')
batch_size = 8
layder_dimension = [2,10,10,10,1]
n_layder = len(layder_dimension)
cur_layder = x
in_dimension = layder_dimension[0]
for i in range(1,n_layder):
    out_dimension = layder_dimension[i]
    weight = get_weight(shape=[in_dimension,out_dimension],lamda=0.001)
    bias = tf.Variable(tf.constant(0.1,shape=[out_dimension]))
    cur_layder = tf.nn.relu(tf.matmul(cur_layder,weight)+bias)
    in_dimension = layder_dimension[i]
mes_loss = tf.reduce_mean(tf.square(y_-cur_layder))
tf.add_to_collection('losses',mes_loss)
loss = tf.add_n(tf.get_collection('losses'))

learning_rate = 0.1
global_ = tf.Variable(tf.constant(0))
decay_steps = batch_size
decay_rate = 0.96
learning_rate = tf.train.exponential_decay(learning_rate, global_, decay_steps, decay_rate, staircase=True)
train_step = tf.train.AdamOptimizer(learning_rate).minimize(loss)

rmd = RandomState(1)
dataset_size = 128
X = rmd.rand(dataset_size,2)
Y = [[int (x1 + x2 < 1)] for (x1,x2) in X]

with tf.Session() as sess:
    init_op = tf.global_variables_initializer()
    sess.run(init_op)
    steps = 5000
    for i in range(steps):
        start = (i*batch_size)%dataset_size
        end = min(start+batch_size,dataset_size)
        sess.run(train_step,feed_dict={x:X[start:end],y_:Y[start:end]})
        if i % 1000 ==0:
            total_cross_entropy = sess.run(loss,feed_dict={x:X,y_:Y})
            print("after %d training step(s),corss entropy on all data is %g"%(i,total_cross_entropy))
    print(sess.run(tf.get_collection('losses'),feed_dict={x:X,y_:Y}))