import tensorflow as tf
w1 = tf.Variable(tf.random_normal([2,3],stddev=1,dtype=tf.float32,seed=1))
w2 = tf.Variable(tf.random_normal([3,1],stddev=1,dtype=tf.float32,seed=1))
x = tf.placeholder(dtype=tf.float32,shape=[3,2])
a = tf.matmul(x,w1)
y = tf.matmul(a,w2)

init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)
print(sess.run(y,feed_dict={x:[[0.7,0.9],[0.1,0.4],[0.5,0.8]]}))
y_ = tf.sigmoid(y)
print(sess.run(y_,feed_dict={x:[[0.7,0.9],[0.1,0.4],[0.5,0.8]]}))
sess.close()