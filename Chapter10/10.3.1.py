import numpy as np
from tensorflow.examples.tutorials.mnist import input_data
import tensorflow as tf

max_features = 20000
max_len = 80
batch_size = 32
tf.logging.set_verbosity(tf.logging.INFO)
dataPata = './MNIST_data/'
mnist = input_data.read_data_sets(dataPata, one_hot=False)

train_X = mnist.train.images
train_Y = mnist.train.labels
test_X = mnist.test.images
test_Y = mnist.test.labels

feature_columns = [tf.feature_column.numeric_column('image', shape=[784])]

estimator = tf.estimator.DNNClassifier(feature_columns=feature_columns, hidden_units=[500],
                                       n_classes=10, optimizer=tf.train.AdamOptimizer(),
                                       model_dir='./Model')
train_input_fn = tf.estimator.inputs.numpy_input_fn(x={'image': mnist.train.images}
                                                    , y=mnist.train.labels.astype(np.int32)
                                                    , num_epochs=None
                                                    , batch_size=128
                                                    , shuffle=True)
estimator.train(input_fn=train_input_fn, steps=100000)
