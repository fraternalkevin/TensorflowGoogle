from keras.layers import Dense, Embedding, LSTM, Input, concatenate
from keras.models import Model
import numpy as np
from tensorflow.examples.tutorials.mnist import input_data
import keras
max_features = 20000
max_len = 80
batch_size = 32
dataPata = './MNIST_data/'
mnist = input_data.read_data_sets(dataPata, one_hot=True)

train_X = mnist.train.images
train_Y = mnist.train.labels
test_X = mnist.test.images
test_Y = mnist.test.labels
input1 = Input(shape=(784,), name='input1')
input2 = Input(shape=(10,), name='input2')

x = Dense(1, activation='relu')(input1)
output1 = Dense(10, activation='softmax', name='output1')(x)

y = concatenate([x, input2])
output2 = Dense(10, activation='softmax', name='output2')(y)

model = Model(inputs=[input1, input2], outputs=[output1, output2])
model.compile(loss=keras.losses.categorical_crossentropy,optimizer=keras.optimizers.SGD(),metrics=['accuracy'])
model.fit({'input1': train_X, 'input2': train_Y}, {'output1': train_Y, 'output2': train_Y}, batch_size=128
          , epochs=20, validation_data=([test_X, test_Y], [test_Y, test_Y]))
